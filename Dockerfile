FROM nodered/node-red-docker
MAINTAINER Denis Grigoryev <dgrigoryev@maprox.net>
RUN \
    npm install node-red-contrib-amqp && \
    npm install node-red-dashboard && \
    npm install node-red-contrib-mongodb2

